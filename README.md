# OpenML dataset: New-York-Citi-Bike-Trip-Duration-2016

https://www.openml.org/d/43573

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
Inspired by the New York City Taxi Trip Duration playground I created a dataset using the publicly available data from this link). Citi Bike is a bike sharing service available in New York City, that permits easy and affordable bike trips. They regularly release data about such trips, including starting and ending stations, starting and ending time, duration of the trip and few others variables.
It closely resembles the data available about taxi trips and I think it could be interesting to compare the two datasets. Let me know if you have any comment.
Content
The dataset covers 4.5M Citi Bike trips from the first 6 months of 2016. The data has been anonymized and the content has been arranged to follow the Taxi Trip dataset categories and nomenclature. 
Notice that the starting and ending point of each trip correspond to one of the 500 Citi Bike stations spread around NYC, most of them in Manhattan, with a substantial subset in Brooklyn.
Acknowledgements
This dataset is the property of NYC Bike Share, LLC and Jersey City Bike Share, LLC (Bikeshare) operates New York Citys Citi Bike bicycle sharing service for TC click here
Inspiration
Is there a correlation between the duration of bike rides and taxi rides? Weather or traffic conditions could affect both in a similar way. 
Is it always faster to get a cab?

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43573) of an [OpenML dataset](https://www.openml.org/d/43573). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43573/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43573/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43573/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

